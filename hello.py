from flask import Flask, jsonify, request
from flask_restful import Resource, Api
import base64
import requests

app = Flask(__name__)
api = Api(app)

@app.route('/')
class Hello(Resource):
  def get(self):
    data = "Hello, World!"
    return jsonify({'data': data})

class Type(Resource):
  def get(self):
    bad_url = request.args["url"]
    url = base64.b64decode(bad_url).decode('utf-8')
    res = requests.head(url)
    return jsonify({'data': res.headers['Content-Type']})

api.add_resource(Hello, "/")
api.add_resource(Type, "/find")

if __name__ == '__main__':
  app.run(debug = True)